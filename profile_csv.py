import matplotlib
matplotlib.use('agg')
import pandas as pd
import pandas_profiling


import argparse

parser = argparse.ArgumentParser(description='Profile the variables in a CSV file and generate a HTML report.')
parser.add_argument("inputfile", help="CSV file to profile")
parser.add_argument("-o", "--output", help="Output report file", default=pandas_profiling.DEFAULT_OUTPUTFILE)

args = parser.parse_args()
df = pd.read_csv(args.inputfile, parse_dates=True)

p = pandas_profiling.ProfileReport(df)
p.to_file(outputfile=args.output)
