## pandas-profiling 

Generates profile reports from a pandas `DataFrame`

For each column the following statistics - if relevant for the column type - are presented in an interactive HTML report:

* **Essentials**:  type, unique values, missing values
* **Quantile statistics** like minimum value, Q1, median, Q3, maximum, range, interquartile range
* **Descriptive statistics** like mean, mode, standard deviation, sum, median absolute deviation, coefficient of variation, kurtosis, skewness
* **Most frequent values**
* **Histogram**
* **Correlations** highlighting of highly correlated variables, Spearman and Pearson matrixes


## Dependencies

* An internet connection
* python (>= 2.7)
* pandas (>=0.19)
* matplotlib  (>=1.4)
* six (>=1.9)



## Run from the command line

For standard formatted CSV files that can be read immediately by pandas, you can use the `profile_csv.py` script. Run

	cd /path/to/Tsuilab_DataProfiling
	python profile_csv.py /path/to/input_file --output /path/to/output_file


	// Example
	python profile_csv.py abalone_example.csv --output abalone_profile.html
	
for information about options and arguments.

## For any errors about missing panda module, you can run the following comamnds:
sudo pip install six --ignore-installed six
sudo pip install matplotlib --upgrade --ignore-installed python-dateutil
sudo pip install pandas jinja2
